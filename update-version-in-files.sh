#!/bin/bash

VERSION=$1
MAJOR=$(echo "$VERSION" | grep -Po '^\d+')
MINOR=$(echo "$VERSION" | sed -re 's@[0-9]+\.([0-9]+)\.[0-9]+@\1@')
PATCH=$(echo "$VERSION" | sed -re 's@^([0-9]+\.){2}@@')

# update meson.build
sed -i -re "s@(project.*)(,\s*version:\s*)'([0-9]+\.){2}[0-9]+@\1${VERSION}@" meson.build


# update README
sed -i -re "s@(^# Vatest).*@\1 ${VERSION}@" README.md
sed -i -re "s@^\s*dependency\('Vatest-[0-9]+\.[0-9]+'\),@\1${MAJOR}.${MINOR}')@" README.md

# update flake
sed -i -re "s@^\s*(version = \").*(\";)@\1${VERSION}\2@g" flake.nix

# update wrap
sed -i -re "s@^(vatest-)[0-9]+\.[0-9]+(.*)@\1${MAJOR}.${MINOR}\2" libvatest.wrap
if [ -n "$CI_COMMIT_TAG" ]; then
  REVISION=$CI_COMMIT_TAG
elif [ $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH ]
  REVISION=$CI_DEFAULT_BRANCH
elif [ $CI_COMMIT_SHA ]
  REVISION=$CI_COMMIT_SHA
else
  REVISION=main
fi

sed -i -re "s@^revision =.*@revision = ${REVISION}" libvatest.wrap
