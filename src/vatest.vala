namespace Vatest {

  public interface MyInterface : Object {

    public abstract string message ();
  }

  public class MyClass : MyInterface, Object {
    public string message () {
      return "vatest";
    }
  }
}
