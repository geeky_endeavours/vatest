# Vatest 0.0.0

A library that reduces the amount of boilerplate code necessary when writing unit tests for a Vala project

## Usage

To use Vatest as a dependency in your project, you can add the following to your project:

1. Create a subproject directory in the root of your project
2. Add a `libvatest.wrap` file from the root of this project, to `subprojects/` directory in the root of the project that should use it
3. In your `meson.build` add this as a dependency and include it in your build:

```meson
dependencies = [
  ...
  dependency('Vatest-0.0')
  ...
]
```

## development

This project comes with a file called `flake.nix` the primary reason for this addition is to be able to bundle dependencies for development in a similar fashion as one does with npm, poetry, cargo and the like. The `flake.lock` file contains the exact versions [nix](https://zero-to-nix.com/) has contrived to use. The `.envrc` files allows [direnv](https://direnv.net/) to setup a development environment using nix packages, that will be torn down again once you cd out of the directory. If you are using Visual Studio Code for development, an official [extension exists to use direnv](https://marketplace.visualstudio.com/items?itemName=mkhl.direnv). 

### Contributing

This project is developed using the node [semantic release](https://github.com/semantic-release/semantic-release#readme) module to manage versions. It is required to use this mode of development when commiting code. It is not difficult, and it is encouraged to spend 10 minutes reading the docs before committing anything here.

Codestyle is enforced in the pipeline using [vala-lint](https://github.com/vala-lang/vala-lint) to keep to a coherent code style. Check the `vala-lint.conf` file in the root of the directory to see the rules setup.

*TL;DR*: use only [angular commit messages convention](https://github.com/angular/angular/blob/main/CONTRIBUTING.md#-commit-message-format) when writing your commit messages


official website: https://gitlab.com/geeky_endeavours/vatest
